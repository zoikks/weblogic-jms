package org.zoikks.weblogic;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * A JMS client is either a publisher or a subscriber or both.
 * 
 * This class is intended to be a helper class for publishing and/or subscribing
 * to JMS topics/queues.
 *
 */
public class JMSClientImpl implements JMSClient {
	
	private static final Logger logger = Logger.getLogger(JMSClientImpl.class);
	
	private InitialContext ctx = null;
    private ConnectionFactory connFactory = null;
    private Connection conn = null;
    private Session session = null;

    private String connFactoryName = "ConnectionFactory-0";
    private String providerUrl = "t3://localhost:7001";
    
    public final static String QUEUE_NAME = "Queue-0";
    public final static String TOPIC_NAME = "Topic-0";
    
    public JMSClientImpl() {

        this.createInitialContext();
        this.createConnectionFactory();
        this.createConnection();

        try {
            this.session = this.conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
        	logger.error("Could not instantiate JMSClientImpl:");
            logger.error(e);
        }
    }
    
    public JMSClientImpl(String providerUrl) {
    	
    	if (StringUtils.isBlank(providerUrl)) {
    		throw new IllegalArgumentException("JMSClientImpl.ctor() - Arguments cannot be null or blank.");
    	}
    	
    	this.providerUrl = providerUrl;
    	
    	this.createInitialContext();
        this.createConnectionFactory();
        this.createConnection();

        try {
            this.session = this.conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
        	logger.error("Could not instantiate JMSClientImpl:");
            logger.error(e);
        }
    }
    
    public JMSClientImpl(String providerUrl, String connFactoryName) {
    	
    	if (StringUtils.isBlank(providerUrl) || StringUtils.isBlank(connFactoryName)) {
    		throw new IllegalArgumentException("JMSClientImpl.ctor() - Arguments cannot be null or blank.");
    	}
    	
    	this.providerUrl = providerUrl;
    	this.connFactoryName = connFactoryName;
    	
    	this.createInitialContext();
        this.createConnectionFactory();
        this.createConnection();

        try {
            this.session = this.conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
        	logger.error("Could not instantiate JMSClientImpl:");
            logger.error(e);
        }
    }
    
    @Override
    public Message receiveMessageFromQueue(String queueName) {
    	
    	Message message = null;
    	
    	try {
            Queue q = this.createQueue(queueName);
            QueueSession qSession = (QueueSession) this.session;
            QueueReceiver qReceiver = qSession.createReceiver(q);
            message = qReceiver.receiveNoWait();
        } catch (JMSException e) {
            logger.error(e);
        }
    	
    	return message;
    }

	@Override
	public void receiveMessagesFromQueue(String queueName,
			MessageListener mListener, ExceptionListener eListener) {
		
		if (mListener == null || eListener == null) {
            throw new IllegalArgumentException(
                                               "JMSConnectionFactory.receiveMessagesFromQueue() - MessageListener and ExceptionListener arguments cannot be null.");
        }

        if (StringUtils.isBlank(queueName)) {
            queueName = QUEUE_NAME;
        }

        try {
            this.conn.setExceptionListener(eListener);
            Queue q = this.createQueue(queueName);
            QueueSession qSession = (QueueSession) this.session;
            QueueReceiver qReceiver = qSession.createReceiver(q);
            qReceiver.setMessageListener(mListener);
        } catch (JMSException e) {
            logger.error(e);
        }
	}

	@Override
	public void receiveMessagesFromTopic(String topicName,
			MessageListener mListener, ExceptionListener eListener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendMessageToQueue(MessageInfo mInfo) throws JMSException {
		if (mInfo == null) {
            throw new IllegalArgumentException("JMSConnectionFactory.sendMessageToQueue() - arguments cannot be null or empty.");
        }

        Message m = this.createMessage(mInfo);

        QueueSender sender = this.createQueueSender(mInfo.getContainerName());
        sender.send(m);

	}

	@Override
	public void sendMessageToTopic(MessageInfo mInfo) throws JMSException {
		
		if (mInfo == null) {
            throw new IllegalArgumentException("JMSConnectionFactory.sendMessageToTopic() - Arguments cannot be null or empty.");
        }

        Message m = this.createMessage(mInfo);

        TopicPublisher publisher = this.createTopicPublisher(mInfo.getContainerName());
        publisher.send(m);
	}

	@Override
	public List<String> browseQueueMessages(String queueName, MessageSelector selector) {
		
		if (StringUtils.isBlank(queueName)) {
            queueName = QUEUE_NAME;
        }

        List<String> messages = new ArrayList<String>();

        try {
            Queue q = this.createQueue(queueName);

            QueueSession qSession = (QueueSession) this.session;

            QueueBrowser qBrowser = null;

            if (selector != null) {
                qBrowser = qSession.createBrowser(q, selector.toString());
            } else {
                qBrowser = qSession.createBrowser(q);
            }

            @SuppressWarnings("rawtypes")
            Enumeration e = qBrowser.getEnumeration();
            int numMsgs = 0;

            while (e.hasMoreElements()) {
                Message m = (Message) e.nextElement();
                String msg = m.toString();
                
                messages.add(msg);
                numMsgs++;
                
                logger.info(msg);
            }

            logger.info("Total Message: " + numMsgs);
        } catch (JMSException e) {
            logger.error(e);
        }

        return messages;
	}

	@Override
	public void startConnection() {
		
		if (this.conn != null) {
            try {
                this.conn.start();
            } catch (JMSException e) {
                logger.error(e);
            }
        }
	}

	@Override
	public void stopConnection() {
		
		if (this.conn != null) {
            try {
                this.conn.stop();
            } catch (JMSException e) {
                logger.error(e);
            }
        }
	}
	
	/**
	 * 
	 */
    private void createInitialContext() {

        Hashtable<String, String> properties = new Hashtable<String, String>();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");

        properties.put(Context.PROVIDER_URL, this.providerUrl);

        try {
            ctx = new InitialContext(properties);
        } catch (NamingException ne) {
            ne.printStackTrace(System.err);
        }
    }
    
    /**
	 * 
	 */
    private void createConnectionFactory() {
        try {
            this.connFactory = (ConnectionFactory) ctx.lookup(this.connFactoryName);
        } catch (NamingException ne) {
            ne.printStackTrace(System.err);
        }
    }
    
    /**
	 * 
	 */
    private void createConnection() {
        try {
            this.conn = this.connFactory.createConnection();
        } catch (JMSException jmse) {
            jmse.printStackTrace(System.err);
        }
    }
	
	/**
     * 
     * @param queueName
     * @return
     * @throws JMSException
     */
    private QueueSender createQueueSender(String queueName) throws JMSException {

        Queue q = this.createQueue(queueName);

        QueueSession qSession = (QueueSession) this.session;
        QueueSender qSender = qSession.createSender(q);

        return qSender;
    }
    
    /**
     * 
     * @param topicName
     * @return
     * @throws JMSException
     */
    private TopicPublisher createTopicPublisher(String topicName) throws JMSException {

        Topic t = this.createTopic(topicName);

        TopicSession tSession = (TopicSession) this.session;
        TopicPublisher tPublisher = tSession.createPublisher(t);

        return tPublisher;
    }
    
    /**
     * 
     * @param queueName
     * @return
     * @throws JMSException
     */
    private Queue createQueue(String queueName) throws JMSException {

        if (StringUtils.isEmpty(queueName)) {
            queueName = QUEUE_NAME;
        }

        try {
            Queue q = (Queue) this.ctx.lookup(queueName);

            return q;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    /**
     * 
     * @param topicName
     * @return
     * @throws JMSException
     */
    private Topic createTopic(String topicName) throws JMSException {

        if (StringUtils.isEmpty(topicName)) {
            topicName = TOPIC_NAME;
        }

        try {
            Topic t = (Topic) this.ctx.lookup(topicName);

            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

	/**
     * 
     * @param mInfo
     * @return
     * @throws JMSException
     */
    private Message createMessage(MessageInfo mInfo) throws JMSException {

        Message m = null;

        if (!mInfo.isBytes()) {
            m = this.createTextMessage(mInfo.getMessage());
        } else {
            m = this.createBytesMessage(mInfo.getMessage());
        }

        if (mInfo.getSelector() != null) {
            m.setStringProperty(mInfo.getSelector().getName(), mInfo.getSelector().getValue());
        }

        return m;
    }
    
    /**
     * 
     * @param message
     * @return
     * @throws JMSException
     */
    private TextMessage createTextMessage(String message) throws JMSException {

        QueueSession qSession = (QueueSession) this.session;
        TextMessage textMessage = qSession.createTextMessage(message);

        return textMessage;
    }

    /**
     * 
     * @param message
     * @return
     * @throws JMSException
     */
    private BytesMessage createBytesMessage(String message) throws JMSException {

        QueueSession qSession = (QueueSession) this.session;
        BytesMessage bytesMessage = qSession.createBytesMessage();
        bytesMessage.writeBytes(message.getBytes());
        bytesMessage.reset();

        return bytesMessage;
    }
}
