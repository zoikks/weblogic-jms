package org.zoikks.weblogic;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * Container class for message type information.
 *
 */
public class MessageInfo {

	private String message;
	private String containerName;
	private boolean bytes;
	private MessageSelector selector;
	
	public MessageInfo(String message, String containerName) {
		
		if (StringUtils.isBlank(message) || StringUtils.isBlank(containerName)) {
			throw new IllegalArgumentException("MessageInfo.ctor() - Arguments cannot be null or empty.");
		}
		
		this.message = message;
		this.containerName = containerName;
		this.bytes = false;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getContainerName() {
		return containerName;
	}
	
	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	
	public boolean isBytes() {
		return bytes;
	}
	
	public void setBytes(boolean bytes) {
		this.bytes = bytes;
	}
	
	public MessageSelector getSelector() {
		return selector;
	}
	
	public void setSelector(MessageSelector selector) {
		this.selector = selector;
	}
}