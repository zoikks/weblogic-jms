package org.zoikks.weblogic;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

/**
 * Sample application for exercising the JMSConnectionFactory.
 */
public class JMSApp implements MessageListener, ExceptionListener {

    private static Logger logger = Logger.getLogger(JMSApp.class);
    
    private JMSClient jmsClient;

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        new JMSApp();
    }

    /**
	 * 
	 */
    private JMSApp() {

        jmsClient = new JMSClientImpl();
        jmsClient.startConnection();
        MessageInfo mInfo = new MessageInfo("bla bla blal", JMSClientImpl.QUEUE_NAME);
        try {
        	jmsClient.receiveMessagesFromQueue(JMSClientImpl.QUEUE_NAME, this, this);
        	jmsClient.sendMessageToQueue(mInfo);
        }
        catch (JMSException e) {
        	logger.error(e);
        }
    }

    /**
	 * 
	 */
    @Override
    public void onMessage(Message message) {

        TextMessage msg = (TextMessage) message;
        try {
            logger.info("received: " + msg.getText());
        } catch (JMSException ex) {
            logger.error(ex);
        }
    }

    /**
	 * 
	 */
    @Override
    public void onException(JMSException e) {

        logger.error(e);
    }
}