package org.zoikks.weblogic;

import java.util.List;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

public interface JMSClient {
	
	public Message receiveMessageFromQueue(String queueName);
	
	public void receiveMessagesFromQueue(String queueName, MessageListener mListener, ExceptionListener eListener);
	
	public void receiveMessagesFromTopic(String topicName, MessageListener mListener, ExceptionListener eListener);

	public void sendMessageToQueue(MessageInfo mInfo) throws JMSException;
	
	public void sendMessageToTopic(MessageInfo mInfo) throws JMSException;
	
	public List<String> browseQueueMessages(String queueName, MessageSelector selector);
	
	public void startConnection();
	
	public void stopConnection();
}