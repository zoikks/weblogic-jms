package org.zoikks.weblogic;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class MessageSelector {

	private String name;
	private String value;
	
	public MessageSelector(String name, String value) {
		
		if (StringUtils.isBlank(name) || StringUtils.isBlank(value)) {
			throw new IllegalArgumentException("MessageSelector.ctor() - Arguments cannot be null or emtpy.");
		}
		
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		
		String string = name + "='" + value + "'";
		
		return string;
	}
	
	@Override
	public boolean equals(Object object) {
		
		if (object == null) {
			return false;
		}
		
		if (object.getClass() != getClass()) {
            return false;
		}
		
		MessageSelector ms = (MessageSelector)object;
		
		if (ms.getName().equals(this.name) &&
				ms.getValue().equals(this.value)) {
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
            append(name).
            append(value).
            toHashCode();
    }
}