package org.zoikks.weblogic;

import java.util.List;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class JMSClientTest extends Assert implements MessageListener, ExceptionListener {

	private static Logger logger = Logger.getLogger(JMSClientTest.class);
	private JMSClient jmsClient;
	
	public JMSClientTest() {
		
		this.jmsClient = new JMSClientImpl();
	}
	
	@Before
	public void setUp() {
		this.jmsClient.startConnection();
		this.clearExistingMessages();
	}
	
	@After
	public void tearDown() {
		this.clearExistingMessages();
		this.jmsClient.stopConnection();
	}
	
	@Test
	public void testSendingMessageToQueue() {
		
		MessageInfo mInfo = new MessageInfo("Test Message", JMSClientImpl.QUEUE_NAME);
		try {
			this.jmsClient.sendMessageToQueue(mInfo);
		}
		catch (JMSException e) {
			logger.error(e);
			fail();
		}
	
		List<String> messages = this.jmsClient.browseQueueMessages(JMSClientImpl.QUEUE_NAME, null);
		
		assertTrue(messages.size() == 1);
	}
	
	private void clearExistingMessages() {

		while (this.jmsClient.browseQueueMessages(JMSClientImpl.QUEUE_NAME, null).size() > 0) {
			Message message = jmsClient.receiveMessageFromQueue(JMSClientImpl.QUEUE_NAME);
			
			logger.debug(message);
		}
	}
	
	@Override
	public void onException(JMSException e) {
		
		logger.error("Receiver.onException: " + e);
	}

	@Override
	public void onMessage(Message message) {
		
		TextMessage msg = (TextMessage) message;
        try {
            logger.info("received: " + msg.getText());
        } catch (JMSException ex) {
            logger.error(ex);
        }
		
	}
}